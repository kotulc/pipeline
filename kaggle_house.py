"""
Test the pipe_baseline.BaselineAggregator class against the Kaggle
Housing Price Competition data set:
    https://www.kaggle.com/c/home-data-for-ml-course
"""


import os
import pandas as pd
import pipe_aggregator as pa

from sklearn.linear_model import Ridge


if __name__ == '__main__':
    # Get the train/test csv file paths in the 'data' folder of the working directory
    test_path = os.path.join(os.getcwd(), 'data\\test.csv')
    train_path = os.path.join(os.getcwd(), 'data\\train.csv')

    # Covert the files to Pandas DataFrame objects
    test_df = pd.read_csv(test_path, index_col='Id')
    train_df = pd.read_csv(train_path, index_col='Id')

    # Remove all rows where the target is NaN or Null
    train_df.dropna(axis=0, subset=['SalePrice'], inplace=True)

    # Create target and mix_input objects
    y = train_df.SalePrice
    X = train_df.drop(columns='SalePrice')

    # Test Aggregator class
    baseline_pipe = pa.PipelineAggregator(Ridge(), X, y)


