"""
Unit test cases for classes of pipe_aggregator.py

    PipelineAggregator
"""


from sklearn.linear_model import LinearRegression

import pandas as pd
import pipe_selector as sl
import pipe_aggregator as pa
import unittest


class PipelineAggregatorTestCase(unittest.TestCase):
    """Test pipe_aggregator.PipelineAggregator class"""

    def setUp(self):
        """Run this setup before each test"""
        cat_dict = {
            'cat1': ['D', 'E', 'D', 'R'], 'cat2': ['C', 'C', 'B', 'A']
        }
        num_dict = {
            'num1': [5, 28, 100, 2], 'num2': [61, 9, 1, 3]
        }
        self.cat_input = pd.DataFrame(cat_dict, columns=cat_dict.keys())
        self.num_input = pd.DataFrame(num_dict, columns=num_dict.keys())
        self.mix_input = self.num_input.join(self.cat_input)
        self.target = pd.Series(data=[0, 2, 5, 6], dtype='int64')
        self.selector = sl.TransformerSelector(
            'mae', LinearRegression(), 'none', train_size=0.5
        )
        self.selector.random_state = 0  # Set train-test split random state

    def test_pipeline_aggregator_return(self):
        """Verify initialization of the aggregator and check score value"""
        pipe_agg = pa.PipelineAggregator(
            LinearRegression(), self.mix_input, self.target
        )
        assert_str = "PipelineAggregator score is None"
        self.assertTrue(pipe_agg.score is not None, assert_str)

    def test_pipeline_aggregator_categorical_return(self):
        """Verify initialization of the aggregator and check score value"""
        pipe_agg = pa.PipelineAggregator(
            LinearRegression(), self.cat_input, self.target
        )
        assert_str = "PipelineAggregator score is None"
        self.assertTrue(pipe_agg.score is not None, assert_str)

    def test_pipeline_aggregator_numerical_return(self):
        """Verify initialization of the aggregator and check score value"""
        pipe_agg = pa.PipelineAggregator(
            LinearRegression(), self.num_input, self.target
        )
        assert_str = "PipelineAggregator score is None"
        self.assertTrue(pipe_agg.score is not None, assert_str)

    def test_pipeline_aggregator_delta_return(self):
        """Verify initialization of the aggregator and check score value"""
        self.selector.min_delta = 0  # Remove delta limiter
        pipe_agg = pa.PipelineAggregator(
            LinearRegression(), self.mix_input, self.target
        )
        assert_str = "PipelineAggregator score is None"
        self.assertTrue(pipe_agg.score is not None, assert_str)
