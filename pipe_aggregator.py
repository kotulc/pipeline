"""
Baseline transformation aggregation:

- Employ the transformations and strategies defined in param_dict
- Pre-processing and transformations performed in listed order
- Save the selected strategy and metadata for re-use and further testing
- Return a sklearn.pipeline.Pipeline object with the selected transformations

Stages: seed, impute, encode, transform, filter (see pipe_stage.py)
"""


from sklearn.pipeline import Pipeline

import numpy as np
import pipe_selector as ps
import pipe_stage as st


DELTA_KEY = 'min_delta'
ENCODE_KEY = 'encode'
FILTER_KEY = 'filter'
IMPUTE_KEY = 'impute'
METRIC_KEY = 'metric'
STAGES_KEY = 'stages'
TRAIN_KEY = 'train_size'
TRANS_KEY = 'transform'

STAGE_MAP = {
    IMPUTE_KEY: st.PipelineImputer,
    ENCODE_KEY: st.PipelineEncoder,
    TRANS_KEY: st.PipelineTransformer,
    FILTER_KEY: st.PipelineFilter
}


class PipelineAggregator(object):
    """Sequentially assemble a Pipeline of transformations"""
    def __init__(self, model, X, y, seed_trans=None, param_dict=None):
        # Default aggregator options
        self.default_params = {
            DELTA_KEY: 0.01,
            METRIC_KEY: ps.SELECTION_METRICS[0],
            STAGES_KEY: (IMPUTE_KEY, ENCODE_KEY, TRANS_KEY, FILTER_KEY),
            TRAIN_KEY: 0.6
        }
        self.score, self.stages, self.selector = None, [], None
        # Initialize stages and selector
        self.param_dict = self.set_params(model, param_dict)
        self.stage_aggregators = self.select_transformers(X, y, seed_trans)
        # Build and test the completed aggregator Pipeline
        agg_pipe = self.get_pipeline()
        self.score = self.selector.evaluate_transformer(agg_pipe)
        print("Aggregator score: {}".format(self.score))

    def get_pipeline(self, model=None):
        """Return the aggregated Pipeline from the stage objects"""
        pipe_steps = []
        # Add each composite transformer from the stage_transformer
        for stage_key, stage_agg in self.stage_aggregators:
            pipe_steps.append((stage_key, stage_agg.transformer))
        if model is not None:
            pipe_steps.append(('model', model))
        return Pipeline(pipe_steps)

    def select_transformers(self, X, y, seed_aggregator=None):
        """Select transformers for each stage and evaluate the results"""
        stage_aggregators = []  # list of StageAggregator objects
        if seed_aggregator is not None:  # Apply seed_transformer to the dataset
            columns = seed_aggregator.output_columns
            dtypes = seed_aggregator.output_dtypes
            stage_aggregators = [('seed', seed_aggregator)]
            X_array = seed_aggregator.transformer.fit_transform(X, y)
        else:  # Convert DataFrame to array
            columns, dtypes = list(X.columns), [d.name for d in X.dtypes]
            X_array = np.array(X)
        y_array = np.array(y).reshape(y.shape[0], 1)  # Convert Series to array
        # Run iterative transformer selection for each pre-processing stage
        for stage_key, stage in self.stages:
            self.selector.selection_data(X_array, y_array)
            stage_agg = stage.select_transformers(columns, dtypes, self.selector)
            if stage_agg is not None:  # StageAggregator returned
                columns = stage_agg.output_columns
                dtypes = stage_agg.output_dtypes
                stage_aggregators.append((stage_key, stage_agg))
                X_array = stage_agg.transformer.fit_transform(X_array, y_array)
        return stage_aggregators

    def set_params(self, model, param_dict=None):
        """Initialize transformer stages and selector from param_dict"""
        if param_dict is None:
            param_dict = self.default_params
        else:  # Ensure required parameters are present
            for param_key, param_default in self.default_params.items():
                if param_key not in param_dict:
                    param_dict[param_key] = param_default
        # Initialize stage transformer options
        for stage_key in param_dict[STAGES_KEY]:
            stage = STAGE_MAP[stage_key](param_dict, stage_key)
            self.stages.append((stage_key, stage))
        # Initialize transformer selector
        self.selector = ps.TransformerSelector(
            metric=param_dict[METRIC_KEY],
            model=model,
            min_delta=param_dict[DELTA_KEY],
            train_size=param_dict[TRAIN_KEY]
        )
        return param_dict