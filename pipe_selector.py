"""
Selection strategies for pipe_stage objects

Selection metrics - Supported regression evaluation metrics

Selection order - Apply column transformations and baseline selection in order
    'none': Do not reorder columns, current column order used
    'f_value': Order column selection by their f-value score
    'mutual_info': Order column selection by their mutual information score
    'random': Randomize column order for both selection and baseline

Selection type - Iterative selection strategy
    'append': Evaluate transformers by appending each transform to the mix_input
    'best': Select the single best transformer applied to all columns
    'replace': Select the best scoring transformation for each column
"""


from sklearn.compose import ColumnTransformer
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline

import sklearn.metrics as sm


MAE_KEY = 'mae'
MSE_KEY = 'mse'
MSLE_key = 'msle'
R2_KEY = 'r2'

METRIC_MAP = {
    MAE_KEY: sm.mean_absolute_error,
    MSE_KEY: sm.mean_squared_error,
    MSLE_key: sm.mean_squared_log_error,
    R2_KEY: sm.r2_score
}

SELECTION_METRICS = (MAE_KEY, MSE_KEY, MSLE_key, R2_KEY)


class TransformerSelector(object):
    """Select transformer objects using the given strategy and metrics"""
    def __init__(self, metric, model, min_delta=0.01, train_size=0.8):
        self.metric = metric  # Error metric
        self.model = model  # Final estimator in the pipeline
        self.min_delta = min_delta  # Minimum difference for selection
        self.train_size = train_size  # Fraction of samples used for training
        self.random_state = None  # set value for non-random Train test split
        self.train_test = None  # Call set_data to initialize

    def evaluate_transformer(self, transformer, metric=None):
        """Return the evaluation score from the transformer"""
        estimator = Pipeline([('eval', transformer), ('model', self.model)])
        X_train, X_test, y_train, y_test = self.train_test
        estimator.fit(X_train, y_train)
        y_predictions = estimator.predict(X_test)
        if metric in SELECTION_METRICS:
            return METRIC_MAP[metric](y_test, y_predictions)
        return METRIC_MAP[self.metric](y_test, y_predictions)

    def select(self, baseline_transformer, eval_transformer, column_indices):
        """Select from the transformers list using the baseline transformer"""
        assert len(column_indices) >= 1
        selected_indices = []
        # Calculate baseline score from model
        base_score = self.evaluate_transformer(baseline_transformer)
        for column_idx in column_indices:
            eval_col_trans = ColumnTransformer([
                ('base', baseline_transformer, column_indices),
                ('eval_col', eval_transformer, [column_idx])
            ])
            eval_score = self.evaluate_transformer(eval_col_trans)
            if eval_score < base_score * (1 - self.min_delta):
                selected_indices.append(column_idx)
        return selected_indices

    def selection_data(self, X, y):
        """Set the training and testing data splits"""
        self.train_test = self.split_data(X, y)

    def split_data(self, X, y):
        """Return the training and testing subsets using train_size"""
        return train_test_split(
            X, y, random_state=self.random_state, train_size=self.train_size
        )
