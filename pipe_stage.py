"""
Pipeline stages: impute, encode, transform, filter

impute - Imputation transforms performed on a per column basis
    'categorical' imputation: most_frequent, constant, knn
    'numerical' imputation: 0, mean, median, knn

encode - Performed on categorical data on a per column basis
    'categorical' encoding: ordinal, sum, target, catboost, hashing

transform - Performed on each selected feature column
    'interaction' transformations: sum, product
    'simple' transformations: square, sqrt, log

filter - Estimate the optimal variance threshold value
    Drop features where variance < threshold (VarianceThreshold)
"""


from sklearn.base import TransformerMixin
from sklearn.compose import ColumnTransformer
from sklearn.feature_selection import VarianceThreshold
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import FunctionTransformer

import category_encoders as ce
import numpy as np

# Split numerical data on the following types
NUMERICAL_DTYPES = ('int32', 'int64', 'float32', 'float64')


class InteractionTransformer(TransformerMixin):
    """Generate permutations of feature products and sums"""
    def __init__(self, column_idx, operation='product'):
        self.column_idx = column_idx
        if operation != 'product':
            assert operation == 'sum'
        self.operation = operation

    def fit(self, X, y=None):
        """Implement TransformerMixin fit method"""
        return self

    def transform(self, X):
        """Apply interaction between column and remainder of mix_input"""
        X_interact = X[:, [self.column_idx]]
        if self.operation == 'sum':
            return X + X_interact
        return X * X_interact

    def get_params(self, deep=False):
        """Required override for estimator class"""
        return {
            'column_idx': self.column_idx,
            'operation': self.operation
        }


class PipelineStage(object):
    """Template class for transformer phase stages"""
    def __init__(self):
        self._default_options = {}  # trans_type: [trans_key, ...]
        self._options = {}  # trans_type: {trans_key: transformer, ...}
        self._selected_options = {}  # trans_type: [(trans_key, transformer), ...]

    def set_options(self, option_dict, stage_key):
        """Add selected transformers from option_dict or _default_options"""
        if stage_key not in option_dict:
            option_dict[stage_key] = self._default_options  # Add all defaults
        for trans_type, opt_list in option_dict[stage_key].items():
            self._selected_options[trans_type] = [
                (t, self._options[trans_type][t]) for t in opt_list
            ]

    def select_transformers(self, columns, dtypes, selector):
        """Evaluate and select transformers from _selected_options"""
        return None  # Return StageTransformer object


class PipelineImputer(PipelineStage):
    """Perform column specific imputation based on model performance"""
    def __init__(self, option_dict, stage_key):
        super().__init__()
        self._default_options = {
            'categorical': ['most_frequent'],
            'numerical': ['zero']
        }
        # Add indicator option in future version
        self._options['categorical'] = {
            'most_frequent': SimpleImputer(strategy='most_frequent'),
            'constant': SimpleImputer(strategy='constant'),
        }
        # 'knn': KNNImputer(n_neighbors=3)
        self._options['numerical'] = {
            'zero': SimpleImputer(strategy='constant'),
            'mean': SimpleImputer(strategy='mean'),
            'median': SimpleImputer(strategy='median'),
        }
        # 'knn': KNNImputer(n_neighbors=3)
        self.set_options(option_dict, stage_key)

    def select_transformers(self, columns, dtypes, selector):
        """Evaluate and select dtype specific transformers"""
        # Split data types into categorical and numerical groups
        cat_indices, num_indices = split_columns(columns, dtypes)
        options_flag = False
        if len(self._selected_options['categorical']) == 0:
            options_flag = True
            print('Warning - PipelineImputer: No categorical options selected.')
        if len(self._selected_options['numerical']) == 0:
            print('Warning - PipelineImputer: No numerical options selected.')
            if options_flag:
                return None
        # Initialize empty transformer collection and evaluate options
        stage_agg = StageAggregator(columns, dtypes)
        stage_agg = self.select_replace(
            cat_indices, self._selected_options['categorical'],
            selector, stage_agg
        )
        stage_agg = self.select_replace(
            num_indices, self._selected_options['numerical'],
            selector, stage_agg
        )
        return stage_agg

    def select_replace(self, columns, key_transformers, selector, stage_agg):
        if len(columns) > 0 and len(key_transformers) > 0:
            # Transformer-column index tracking
            baseline_columns = TransformerColumns(columns, key_transformers[0])
            # Evaluate columns for each remaining transformer
            for trans_idx, (key, transformer) in enumerate(key_transformers):
                base_trans = baseline_columns.transformer
                trans_columns = selector.select(base_trans, transformer, columns)
                if len(trans_columns) > 0:
                    for column_idx in trans_columns:
                        baseline_columns.replace(column_idx, key, transformer)
            for key, trans, cols in baseline_columns.key_transformer_columns:
                stage_trans = StageTransformer(cols, key, trans)
                stage_agg.append(stage_trans)
        return stage_agg


class PipelineEncoder(PipelineStage):
    """Encode categorical features and return the combined feature set"""
    def __init__(self, option_dict, stage_key):
        super().__init__()
        self._default_options = {'categorical': ['ordinal']}
        self._options['categorical'] = {
            'ordinal': ce.ordinal.OrdinalEncoder(return_df=False),
            'sum': ce.sum_coding.SumEncoder(return_df=False),
            'target': ce.target_encoder.TargetEncoder(return_df=False),
            'catboost': ce.cat_boost.CatBoostEncoder(return_df=False),
            'hashing': ce.hashing.HashingEncoder(return_df=False)
        }
        self.set_options(option_dict, stage_key)

    def select_transformers(self, columns, dtypes, selector):
        """Evaluate and select transformers from _selected_options"""
        cat_indices, num_indices = split_columns(columns, dtypes)
        # Initialize empty transformer collection and evaluate options
        stage_agg = StageAggregator(columns, dtypes)
        if len(cat_indices) == 0:
            print('Warning - PipelineEncoder: no categorical data to encode.')
            return None  # No categorical data to encode
        if len(num_indices) > 0:  # Pass numeric data through by default
            stage_trans = StageTransformer(num_indices, 'pass', 'passthrough')
            stage_agg.append(stage_trans)
        if len(self._selected_options['categorical']) == 0:
            raise ValueError('PipelineEncoder: no encoding options selected.')
        # Append encoded columns that improve the model performance
        key_transformers = self._selected_options['categorical']
        for key, transformer in key_transformers:
            base_trans = stage_agg.transformer
            trans_columns = selector.select(base_trans, transformer, columns)
            if len(trans_columns) > 0:
                stage_trans = StageTransformer(
                    trans_columns, key, transformer, dtype='float64'
                )
                stage_agg.append(stage_trans)
        return stage_agg


class PipelineTransformer(PipelineStage):
    """Generate new features from transformations and interactions"""
    def __init__(self, option_dict, stage_key):
        super().__init__()
        self._default_options = {
            'interaction': ['product'],
            'simple': ['log']
        }
        # one_hot interaction option in future version
        self._options['interaction'] = {
            'product': InteractionTransformer('product'),
            'sum': InteractionTransformer('sum')
        }
        self._options['simple'] = {
            'log': FunctionTransformer(np.log1p, validate=True),
            'square': FunctionTransformer(np.sqrt, validate=True),
            'sqrt': FunctionTransformer(np.square, validate=True)
        }
        self.set_options(option_dict, stage_key)

    def select_transformers(self, columns, dtypes, selector):
        """Evaluate and select transformers from _selected_options"""
        cat_indices, num_indices = split_columns(columns, dtypes)
        if len(cat_indices) > 0:  # Print warning if dates/objects detected
            print('Warning - PipelineTransformer: categorical dtypes ignored.')
        if len(num_indices) == 0:  # Pass numeric data through by default
            raise ValueError('PipelineTransformer: no numerical features.')
        if len(self._selected_options) == 0:
            raise ValueError('PipelineTransformer: no options selected.')
        # Initialize transformers collection
        stage_agg = StageAggregator(num_indices, dtypes)
        stage_trans = StageTransformer(num_indices, 'pass', 'passthrough')
        stage_agg.append(stage_trans)
        if 'simple' in self._selected_options:
            key_transformers = self._selected_options['simple']
            for key, transformer in key_transformers:
                base_trans = stage_agg.transformer
                trans_columns = selector.select(base_trans, transformer, columns)
                if len(trans_columns) > 0:
                    stage_trans = StageTransformer(
                        trans_columns, key, transformer
                    )
                    stage_agg.append(stage_trans)
        if 'interaction' in self._selected_options:
            for key, _ in self._selected_options['interaction']:
                for column_idx in num_indices:
                    trans_key = columns[column_idx] + '_' + key
                    transformer = InteractionTransformer(column_idx, key)
                    base_trans = stage_agg.transformer
                    trans_columns = selector.select(
                        base_trans, transformer, columns
                    )
                    if len(trans_columns) > 0:
                        input_columns = [column_idx] + trans_columns
                        stage_trans = StageTransformer(
                            input_columns, trans_key, transformer,
                            output=trans_columns
                        )
                        stage_agg.append(stage_trans)
        return stage_agg


class PipelineFilter(PipelineStage):
    """Drops features that do not meet variance threshold"""
    def __init__(self, option_dict, stage_key):
        super().__init__()
        # Options to test a series of variance threshold values
        self._default_options = {'variance': ['10']}
        self._options['variance'] = {
            '5': np.linspace(0, 0.9, 5),
            '10': np.linspace(0, 0.9, 10),
            '20': np.linspace(0, 0.9, 20),
        }
        self.set_options(option_dict, stage_key)

    def select_transformers(self, columns, dtypes, selector):
        """Evaluate selected variance threshold values"""
        cat_indices, num_indices = split_columns(columns, dtypes)
        if len(cat_indices) > 0:  # Print warning if dates/objects detected
            print('Warning - PipelineFilter: categorical dtypes ignored.')
        if len(num_indices) == 0:  # Pass numeric data through by default
            raise ValueError('PipelineFilter: no numerical features.')
        if len(self._selected_options) == 0:
            raise ValueError('PipelineFilter: no options selected.')
        # Initialize transformers collection
        stage_agg = StageAggregator(num_indices, dtypes)
        base_key = 'pass'
        base_trans = ColumnTransformer([(base_key, 'passthrough', num_indices)])
        _, thresholds = self._selected_options['variance'][0]
        base_score = selector.evaluate_transformer(base_trans)
        for threshold in thresholds:
            key = 'var-{:0.2f}'.format(threshold)
            transformer = VarianceThreshold(threshold)
            trans_score = selector.evaluate_transformer(transformer)
            if trans_score < base_score:
                base_key, base_score, base_trans = key, trans_score, transformer
        if base_key == 'pass':
            return None
        stage_trans = StageTransformer(num_indices, base_key, base_trans)
        stage_agg.append(stage_trans)
        return stage_agg


class StageTransformer(object):
    """Container for input, key, transformer, dtype and output"""
    def __init__(self, input, key, transformer, dtype=None, output=None):
        self.input_indices = input
        self.key = key
        self.transformer = transformer
        self.output_dtype = dtype
        self.output_indices = output
        if output is None:
            self.output_indices = input

    @property
    def column_transformer(self):
        """Return (key, transformer, column_indices) tuple"""
        return self.key, self.transformer, self.input_indices


class StageAggregator(object):
    """Column tracking and pipeline transformer composition"""
    def __init__(self, columns, dtypes='float64'):
        # Transforms applied in order of stage_transformers
        self.columns = columns  # Input column labels
        self.dtypes = dtypes  # Input column dtypes
        self.stage_transformers = []  # List of StageTransformer objects

    def __getitem__(self, item_idx):
        """Return the StageTransformer at index item_idx"""
        return self.stage_transformers[item_idx]

    def __len__(self):
        """Return the number of StageTransformer objects"""
        return len(self.stage_transformers)

    @property
    def output_columns(self):
        """Return the labels for all aggregate output columns"""
        output_columns = []
        for stage_trans in self.stage_transformers:
            s = '_' + stage_trans.key
            trans_out = [self.columns[i] + s for i in stage_trans.output_indices]
            output_columns.extend(trans_out)
        return output_columns

    @property
    def output_dtypes(self):
        """Return the dtypes for all aggregate output columns"""
        output_dtypes = []
        for stage_trans in self.stage_transformers:
            dtypes = [stage_trans.output_dtype] * len(stage_trans.output_indices)
            output_dtypes.extend(dtypes)
        return output_dtypes

    @property
    def transformer(self):
        """Return a single or composite transformer"""
        if len(self.stage_transformers) == 0:
            return None
        if len(self.stage_transformers) == 1:
            return self.stage_transformers[0].transformer
        transformer_list = []
        for stage_trans in self.stage_transformers:  # Add each transformer
            transformer_list.append(stage_trans.column_transformer)
        return ColumnTransformer(transformer_list)

    def append(self, stage_transformer):
        """Append the list of stage transformers"""
        if stage_transformer.output_dtype is None:
            # Assign first dtype from the transformers input columns
            input_column = stage_transformer.input_indices[0]
            stage_transformer.output_dtype = self.dtypes[input_column]
        self.stage_transformers.append(stage_transformer)


class TransformerColumns(object):
    """One to one column to transformer assignment"""
    def __init__(self, columns, key_transformer):
        self.column_trans_dict = {c_idx: key_transformer for c_idx in columns}

    @property
    def key_transformer_columns(self):
        """Return list of (key, transformer, columns)"""
        rev_dict = {v[0]: (v[1], []) for v in self.column_trans_dict.values()}
        for column_idx in self.column_trans_dict.keys():
            trans_key = self.column_trans_dict[column_idx][0]
            rev_dict[trans_key][1].append(column_idx)
        return [(k, v[0], v[1]) for k, v in rev_dict.items()]

    @property
    def transformer(self):
        """Return a single or composite transformer object"""
        key_trans_col_list = self.key_transformer_columns
        if len(key_trans_col_list) == 0:
            return None
        if len(key_trans_col_list) == 1:
            return key_trans_col_list[1]
        return ColumnTransformer(key_trans_col_list)

    def replace(self, column_idx, key, transformer):
        """Replace the (key, transformer) pair at column_idx"""
        self.column_trans_dict[column_idx] = (key, transformer)


def split_columns(columns, dtypes):
    """Return a list of categorical and numerical column indices"""
    cat_columns = [i for i in range(len(columns)) if dtypes[i] == 'object']
    num_columns = [
        i for i in range(len(columns)) if dtypes[i] in NUMERICAL_DTYPES
    ]
    return cat_columns, num_columns
