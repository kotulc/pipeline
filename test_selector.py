"""
Unit test cases for classes of pipe_selector.py

    ColumnAggregator
    TransformerSelector
"""


from sklearn.linear_model import Ridge
from sklearn.preprocessing import FunctionTransformer

import numpy as np
import pipe_selector as ps
import pipe_stage as st
import unittest


class SelectorTestCase(unittest.TestCase):
    """Test replace function"""
    def setUp(self):
        """Run this setup before each test"""
        key_trans_list = [
            ('log', FunctionTransformer(np.log1p)),
            ('square', FunctionTransformer(np.square))
        ]
        base_trans = ('pass', 'passthrough', [0, 1])
        self.stage_agg = st.StageAggregator(
            ['col1', 'col2'], ['int64', 'int64'], base_trans
        )
        self.stage_agg.extend([0, 1], key_trans_list)

    def test_replace_none_list_update(self):
        """Verify baseline indices replaced"""
        column_list, trans_idx = [None, None], 1
        ps.replace(0, column_list, self.stage_agg, trans_idx)
        expected_labels, expected_list = ['col2', 'col1_square'], [1, None]
        assert_str = "Expected and returned values do not match"
        self.assertEqual(
            expected_labels, self.stage_agg.output_columns, assert_str
        )
        self.assertEqual(expected_list, column_list, assert_str)

    def test_replace_assigned_list_update(self):
        """Verify baseline indices replaced"""
        column_list = [None, None]
        ps.replace(0, column_list, self.stage_agg, 1)
        ps.replace(0, column_list, self.stage_agg, None)
        expected_labels, expected_list = ['col1', 'col2'], [None, None]
        assert_str = "Expected and returned values do not match"
        self.assertEqual(
            expected_labels, self.stage_agg.output_columns, assert_str
        )
        self.assertEqual(expected_list, column_list, assert_str)


class TransformerSelectorTestCase(unittest.TestCase):
    """Test pipe_selector.TransformerSelector class"""
    def setUp(self):
        """Run this setup before each test"""
        train_input = np.array([[5, 28], [61, 9]])
        test_input = np.array([[100, 2], [3, 40]])
        train_out, test_out = np.array([0, 2]), np.array([5, 6])
        self.base_score = 10  # Baseline score
        self.train_test = [train_input, test_input, train_out, test_out]
        self.selector = ps.TransformerSelector(
            'mae', Ridge(random_state=0), 'none', train_size=0.5
        )
        self.key_trans_list = [
            ('log', FunctionTransformer(np.log1p)),
            ('square', FunctionTransformer(np.square))
        ]
        base_trans = ('pass', 'passthrough', [0, 1])
        self.stage_agg = st.StageAggregator(
            ['col1', 'col2'], ['int64', 'int64'], base_trans
        )
        self.stage_agg.extend([0, 1], self.key_trans_list)

    def test_select_append_return(self):
        """Verify log and square transforms appended to baseline out"""
        expected = ['col1', 'col2', 'col1_log', 'col1_square']
        stage_agg = self.selector.select_append(
            self.base_score, self.stage_agg, self.train_test
        )
        assert_str = "Expected and returned values do not match"
        self.assertEqual(expected, stage_agg.output_columns, assert_str)

    def test_select_best_return(self):
        """Verify selection of 'square' transformer"""
        expected = ['col1_square', 'col2_square']
        stage_agg = self.selector.select_best(
            self.base_score, self.stage_agg, self.train_test
        )
        assert_str = "Baseline transformer is not disabled"
        self.assertTrue(self.stage_agg.base_transformer is None, assert_str)
        assert_str = "Expected and returned values do not match"
        self.assertEqual(expected, stage_agg.output_columns, assert_str)

    def test_select_replace_return(self):
        """Verify selection of 'square' transformer"""
        stage_agg = self.selector.select_replace(
            self.base_score, self.stage_agg, self.train_test
        )
        assert_str = "Expected and returned values do not match"
        expected, returned = [1], stage_agg[0].output_indices
        self.assertEqual(expected, returned, assert_str)
        expected, returned = [0], stage_agg[1].output_indices
        self.assertEqual(expected, returned, assert_str)
        expected = ['col2_log', 'col1_square']
        returned = stage_agg.output_columns
        self.assertEqual(expected, returned, assert_str)


if __name__ == '__main__':
    unittest.main()
