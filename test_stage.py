"""
Unit test cases for classes of pipe_stage.py

    PipelineStage
        PipelineImputer
        PipelineEncoder
        PipelineTransformer
        PipelineFilter
    Transformer
        InteractionTransformer
        PassthroughTransformer
"""


from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import FunctionTransformer

import numpy as np
import pipe_selector as sl
import pipe_stage as ps
import unittest


class StageAggregatorTestCase(unittest.TestCase):
    """Test pipe_stage.PipelineStage class"""
    def setUp(self):
        """Run this setup before each test"""
        base_trans = ('base', 'passthrough', [0, 1])
        eval_trans = ('eval', 'passthrough', [0])
        columns = ['num', 'cat']
        dtypes = ['int64', 'object']
        self.key_trans_list = [
            ('log', FunctionTransformer(np.log1p)),
            ('square', FunctionTransformer(np.square))
        ]
        self.stage_agg = ps.StageAggregator(
            columns, dtypes, base_trans, eval_trans
        )
        self.stage_agg.extend([0], self.key_trans_list)

    def test_enabled_transformers(self):
        self.stage_agg[0].enable_output()
        assert_str = "Missing assigned stage transformers"
        self.assertEqual(len(self.stage_agg), 2, assert_str)
        enabled_transformers = len(self.stage_agg.enabled_transformers)
        assert_str = "Stage transformer at index 0 not enabled"
        self.assertEqual(enabled_transformers, 1, assert_str)

    def test_output_columns(self):
        self.stage_agg[0].enable_output()
        expected = ['num', 'cat', 'num_log']
        assert_str = "Expected and returned output columns do not match"
        self.assertEqual(expected, self.stage_agg.output_columns, assert_str)

    def test_output_dtypes(self):
        self.stage_agg[0].enable_output()
        expected = ['int64', 'object', 'int64']
        assert_str = "Expected and returned output dtypes do not match"
        self.assertEqual(expected, self.stage_agg.output_dtypes, assert_str)

    def test_transformer(self):
        self.stage_agg[0].enable_output()
        col_trans_len = len(self.stage_agg.transformer.transformers)
        assert_str = "Column transformer length does not match expected length"
        self.assertEqual(col_trans_len, 2, assert_str)


class PipelineStageTestCase(unittest.TestCase):
    """Test pipe_stage.PipelineStage class"""
    def setUp(self):
        """Run this setup before each test"""
        self.columns = ['num', 'cat']
        self.dtypes = ['int64', 'object']
        num_input = np.array([[5], [28], [61], [9]])
        cat_input = np.array([['D'], ['E'], ['R'], ['D']]).astype(np.object)
        self.mix_input = np.hstack((num_input, cat_input))
        self.target = np.array([[0], [2], [5], [6]])
        self.selector = sl.TransformerSelector(
            'mae', LinearRegression(), 'none', min_delta=0, train_size=0.5
        )
        self.selector.random_state = 0  # Set train-test split random state

    def test_pipeline_encoder_select_transformers_return(self):
        """Verify returned transformer contains cat/pass transformers"""
        pipe_encoder = ps.PipelineEncoder({}, 'encode')
        stage_agg = pipe_encoder.select_transformers(
            self.columns, self.dtypes, self.selector,
            self.mix_input, self.target
        )
        expected = ['num', 'cat_ordinal']
        returned = stage_agg.output_columns
        assert_str = "Expected and returned output dtypes do not match"
        self.assertEqual(expected, returned, assert_str)

    def test_pipeline_filter_select_transformers_return(self):
        """Verify that a single transformer is returned"""
        pipe_filter = ps.PipelineFilter({}, 'filter')
        stage_agg = pipe_filter.select_transformers(
            self.columns, self.dtypes, self.selector,
            self.mix_input, self.target
        )
        trans_count = len(stage_agg.enabled_transformers)
        assert_str = "Returned transformer list not empty"
        self.assertEqual(trans_count, 0, assert_str)
        assert_str = "Baseline transformer is None"
        self.assertTrue(stage_agg.base_transformer is not None, assert_str)

    def test_pipeline_imputer_select_transformers_return(self):
        """Verify returned transformer contains cat/num transformers"""
        pipe_imputer = ps.PipelineImputer({}, 'impute')
        stage_agg = pipe_imputer.select_transformers(
            self.columns, self.dtypes, self.selector,
            self.mix_input, self.target
        )
        expected = ['cat', 'num_zero']
        returned = stage_agg.output_columns
        assert_str = "Expected and returned output dtypes do not match"
        self.assertEqual(expected, returned, assert_str)

    def test_pipeline_stage_set_options(self):
        """Confirm that default options are initialized"""
        pipe_stages = [
            ps.PipelineImputer({}, 'impute'),
            ps.PipelineEncoder({}, 'encode'),
            ps.PipelineTransformer({}, 'transform'),
            ps.PipelineFilter({}, 'filter')
        ]
        has_options = [s for s in pipe_stages if len(s._selected_options) > 0]
        assert_str = "Stage contains no selected options"
        self.assertEqual(len(has_options), 4, assert_str)

    def test_pipeline_transformer_select_transformers_interaction_return(self):
        """Verify returned transformer has at least two valid columns"""
        option_dict = {'transform': {'interaction': ['product']}}
        pipe_trans = ps.PipelineTransformer(option_dict, 'transform')
        stage_agg = pipe_trans.select_transformers(
            self.columns, self.dtypes, self.selector,
            self.mix_input, self.target
        )
        enabled_len = len(stage_agg.enabled_transformers)
        assert_str = "enabled_transformers length should be 0"
        self.assertTrue(enabled_len == 0, assert_str)

    def test_pipeline_transformer_select_transformers_simple_return(self):
        """Verify returned transformer has at least two valid columns"""
        option_dict = {'transform': {'simple': ['log', 'square']}}
        pipe_trans = ps.PipelineTransformer(option_dict, 'transform')
        stage_agg = pipe_trans.select_transformers(
            self.columns, self.dtypes, self.selector,
            self.mix_input, self.target
        )
        enabled_len = len(stage_agg.enabled_transformers)
        assert_str = "enabled_transformers length should be 0"
        self.assertTrue(enabled_len == 0, assert_str)


class TransformerTestCase(unittest.TestCase):
    """Test Interaction and Passthrough transformer classes"""
    def setUp(self):
        """Run this setup before each test"""
        self.num_input = np.array([[5, 28, 100, 2], [61, 9, 1, 3]])

    def test_interaction_transformer_return(self):
        """Verify that the product interaction has been applied"""
        it_transformer = ps.InteractionTransformer(0)
        returned = it_transformer.transform(self.num_input)
        assert_str = "Interaction transform not applied"
        self.assertEqual(returned[0, 0], 25, assert_str)